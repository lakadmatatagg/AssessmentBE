package com.sapura.assessment.repository;

import com.sapura.assessment.entity.TimesheetNoJoin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimesheetNoJoinRepo extends JpaRepository<TimesheetNoJoin, Integer> {
}
