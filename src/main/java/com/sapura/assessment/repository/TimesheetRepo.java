package com.sapura.assessment.repository;

import com.sapura.assessment.entity.Timesheet;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TimesheetRepo extends JpaRepository<Timesheet, Integer> {

    // ByTask
    List<Timesheet> findByTaskContaining(String task, Sort sort);
    List<Timesheet> findByTaskContainingOrderByDateFromAsc(String task);
    List<Timesheet> findByTaskContainingOrderByDateFromDesc(String task);
    List<Timesheet> findByTaskContainingOrderByDateToAsc(String task);
    List<Timesheet> findByTaskContainingOrderByDateToDesc(String task);
    List<Timesheet> findByTaskContainingOrderByProjectAsc(String task);
    List<Timesheet> findByTaskContainingOrderByProjectDesc(String task);
    List<Timesheet> findByTaskContainingOrderByTaskAsc(String task);
    List<Timesheet> findByTaskContainingOrderByTaskDesc(String task);

    // All
    List<Timesheet> findAllByOrderByDateFromAsc();
    List<Timesheet> findAllByOrderByDateFromDesc();
    List<Timesheet> findAllByOrderByDateToAsc();
    List<Timesheet> findAllByOrderByDateToDesc();

//    @Query("SELECT new com.sapura.assessment.dto.TimesheetRespond(u.name, s.status_name) FROM timesheet t JOIN t.user u JOIN t.status s")
//    public List<TimesheetRespond> getTimesheetProper();
}
