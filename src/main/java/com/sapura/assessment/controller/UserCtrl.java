package com.sapura.assessment.controller;

import com.sapura.assessment.entity.User;
import com.sapura.assessment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
public class UserCtrl {

    @Autowired
    private UserService service;

    @GetMapping("/users")
    public List<User> findAllUser() {
        return service.getUser();
    }
}
