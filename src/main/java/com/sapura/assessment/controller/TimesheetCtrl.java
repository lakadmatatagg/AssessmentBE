package com.sapura.assessment.controller;

//import com.sapura.assessment.dto.TimesheetRespond;
import com.sapura.assessment.entity.Timesheet;
import com.sapura.assessment.repository.TimesheetRepo;
import com.sapura.assessment.service.TimesheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class TimesheetCtrl {

    @Autowired
    private TimesheetService service;

    @PostMapping("/addTimesheet")
    public Timesheet addTimesheet(@RequestBody Timesheet timesheet) {
        return service.saveTimesheet(timesheet);
    }

    @GetMapping("/timesheets")
    public List<Timesheet> findAllTimesheet() {
        return service.getTimesheets();
    }

    @GetMapping("/timesheets/{field}/by/{order}")
    public List<Timesheet> findAllTimesheetsSorted(@PathVariable String field, @PathVariable String order) {
        return service.getTimesheetsSorted(field, order);
    }

//    @GetMapping("/timesheetsFull")
//    public List<TimesheetRespond> findProperTimesheet() {
//        return service.getTimesheetsProper();
//    }

    @GetMapping("/timesheet/{id}")
    public Timesheet findTimeSheetById(@PathVariable int id) { return service.getTimesheetsById(id); }

    @GetMapping("/timesheetByTask/{task}")
    public ResponseEntity<List<Timesheet>> findTimeSheetByTask(@PathVariable String task) { return service.getTimesheetsByTask(task); }

    @GetMapping("/timesheetByTaskSorted/{task}/field/{field}/order/{order}")
    public ResponseEntity<List<Timesheet>> findTimeSheetByTaskSorted(@PathVariable String task, @PathVariable String field, @PathVariable String order) { return service.getTimesheetsByTaskSorted(task, field, order); }

    @PutMapping("/update")
    public Timesheet updateTimesheet(@RequestBody Timesheet timesheet) {
        return service.updateTimesheet(timesheet);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteTimesheet(@PathVariable int id) {
        System.out.println(id);
        return service.deleteTimesheet(id);
    }
}
