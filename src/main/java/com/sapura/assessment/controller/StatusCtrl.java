package com.sapura.assessment.controller;

import com.sapura.assessment.entity.Status;
import com.sapura.assessment.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
public class StatusCtrl {

    @Autowired
    private StatusService service;

    @GetMapping("/status")
    public List<Status> findAllStatus() { return service.getStatus(); }
}
