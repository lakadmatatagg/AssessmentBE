package com.sapura.assessment.service;

import com.sapura.assessment.entity.Timesheet;
import com.sapura.assessment.entity.User;
import com.sapura.assessment.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepo repository;

        public User getUserById(int id) {
            return repository.getReferenceById(id);
        }

        public List<User> getUser() {
            return repository.findAll();
        }
}
