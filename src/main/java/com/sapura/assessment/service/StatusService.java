package com.sapura.assessment.service;

import com.sapura.assessment.entity.Status;
import com.sapura.assessment.repository.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusService {

    @Autowired
    private StatusRepo repository;

    public List<Status> getStatus() {
        return repository.findAll();

    }
}
