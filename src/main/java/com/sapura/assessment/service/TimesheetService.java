package com.sapura.assessment.service;

import com.sapura.assessment.entity.Timesheet;
import com.sapura.assessment.repository.TimesheetNoJoinRepo;
import com.sapura.assessment.repository.TimesheetRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TimesheetService {
    @Autowired
    private TimesheetRepo repository;
    @Autowired
    private TimesheetNoJoinRepo repositoryNoJoin;

    public Timesheet saveTimesheet(Timesheet timesheet) {
        return repository.save(timesheet);
    }

    public List<Timesheet> getTimesheets() {
        return repository.findAll();
    }

    public List<Timesheet> getTimesheetsSorted(String field, String order) {
        if (field.equalsIgnoreCase("date_from")) {
            if (order.equalsIgnoreCase("ASC")) {
                return repository.findAllByOrderByDateFromAsc();
            } else {
                return repository.findAllByOrderByDateFromDesc();
            }
        } else if (field.equalsIgnoreCase("date_to")) {
            if (order.equalsIgnoreCase("ASC")) {
                return repository.findAllByOrderByDateToAsc();
            } else {
                return repository.findAllByOrderByDateToDesc();
            }
        } else if (field.equalsIgnoreCase("assign_to")) {
            if (order.equalsIgnoreCase("ASC")) {
                return repository.findAll(Sort.by(Sort.Direction.ASC, "users_Name"));
            } else {
                return repository.findAll(Sort.by(Sort.Direction.DESC,"users_Name"));
            }
        } else if (field.equalsIgnoreCase("status")) {
            if (order.equalsIgnoreCase("ASC")) {
                return repository.findAll(Sort.by(Sort.Direction.ASC, "statuses_StatusName"));
            } else {
                return repository.findAll(Sort.by(Sort.Direction.DESC,"statuses_StatusName"));
            }
        } else {
            if (order.equalsIgnoreCase("ASC")) {
                return repository.findAll(Sort.by(Sort.Direction.ASC,field));
            } else {
                return repository.findAll(Sort.by(Sort.Direction.DESC,field));
            }
        }
    }

    public Timesheet getTimesheetsById(int id) {
        return repository.findById(id).orElse(null);
    }

    public ResponseEntity<List<Timesheet>> getTimesheetsByTask(String task) {
        return new ResponseEntity<List<Timesheet>>(repository.findByTaskContaining(task, Sort.by(Sort.Direction.ASC, "task")), HttpStatus.OK);
    }

    public ResponseEntity<List<Timesheet>> getTimesheetsByTaskSorted(String task, String field, String order) {

        if (field.equalsIgnoreCase("date_from")) {
            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByDateFromAsc(task), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByDateFromDesc(task), HttpStatus.OK);
            }
        }  else if (field.equalsIgnoreCase("date_to")) {
            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByDateToAsc(task), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByDateToDesc(task), HttpStatus.OK);
            }
        } else if (field.equalsIgnoreCase("project")) {
            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByProjectAsc(task), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByProjectDesc(task), HttpStatus.OK);
            }
        } else if (field.equalsIgnoreCase("assign_to")) {

            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContaining(task, Sort.by(Sort.Direction.ASC, "users_Name")), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContaining(task, Sort.by(Sort.Direction.DESC, "users_Name")), HttpStatus.OK);
            }
        } else if (field.equalsIgnoreCase("status")) {
            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContaining(task, Sort.by(Sort.Direction.ASC, "statuses_StatusName")), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContaining(task, Sort.by(Sort.Direction.DESC, "statuses_StatusName")), HttpStatus.OK);
            }
        } else {
            if (order.equalsIgnoreCase("ASC")) {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByTaskAsc(task), HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Timesheet>>(repository.findByTaskContainingOrderByTaskDesc(task), HttpStatus.OK);
            }
        }
    }
    public String deleteTimesheet(int id) {
        repositoryNoJoin.deleteById(id);
        return "Removed : "+id;
    }

    public Timesheet updateTimesheet(Timesheet timesheet) {
        Timesheet existingts = repository.findById(timesheet.getId()).orElse(null);
        existingts.setProject(timesheet.getProject());
        existingts.setTask(timesheet.getTask());
        existingts.setDateFrom(timesheet.getDateFrom());
        existingts.setDateTo(timesheet.getDateTo());
        existingts.setStatus_id(timesheet.getStatus_id());
        existingts.setUser_id(timesheet.getUser_id());
        return repository.save(existingts);
    }
}
