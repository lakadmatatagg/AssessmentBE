package com.sapura.assessment.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "timesheet")
public class Timesheet {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String project;
    private String task;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;
    private int user_id;
    private int status_id;

    @OneToMany(targetEntity = User.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id", referencedColumnName = "user_id")
    private List<User> users;

    @OneToMany(targetEntity = Status.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id", referencedColumnName = "status_id")
    private List<Status> statuses;
}
